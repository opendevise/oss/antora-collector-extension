/* eslint-env mocha */
'use strict'

process.env.NODE_ENV = 'test'

const assert = require('node:assert/strict')
const { describe, before, beforeEach, after, afterEach, it } = require('node:test')

const { configureLogger } = require('@antora/logger')
const fs = require('node:fs')
const fsp = require('node:fs/promises')
const { Git: GitServer } = require('node-git-server')
const { once } = require('node:events')
const { spawnSync } = require('node:child_process')
const yaml = require('js-yaml')

beforeEach(() => configureLogger({ level: 'silent' }))

const assertx = {
  contents: (actual, expected, msg) => {
    assertx.file(actual)
    assert.equal(fs.readFileSync(actual, 'utf8'), expected, msg)
  },
  deepEqualSlice: (actual, expected, msg) =>
    assert.deepEqual(sliceObject(actual, ...Object.keys(expected)), expected, msg),
  directory: (actual) => {
    let stat
    try {
      stat = fs.statSync(actual)
    } catch {
      stat = { isDirectory: () => false }
    }
    assert(stat.isDirectory(), `Expected value to be a directory: ${actual}`)
  },
  empty: (actual) => assert.equal(actual.length, 0, 'Expected value to be empty'),
  file: (actual) => {
    let stat
    try {
      stat = fs.statSync(actual)
    } catch {
      stat = { isFile: () => false }
    }
    assert(stat.isFile(), `Expected value to be a file: ${actual}`)
  },
}

// NOTE must use setImmediate to flush any queued operations that will write to stream
// see https://nodejs.org/api/test.html#extraneous-asynchronous-activity
function captureStdStream (stream, fn) {
  return new Promise((resolve, reject) => {
    // TODO could write this without async flag
    setImmediate(async () => {
      const streamWrite = stream.write
      try {
        const data = []
        stream.write = (buffer) => data.push(buffer)
        await fn()
        resolve(data.join(''))
      } catch (err) {
        reject(err)
      } finally {
        stream.write = streamWrite
      }
    })
  })
}

const captureStderr = captureStdStream.bind(null, process.stderr)

const captureStdout = captureStdStream.bind(null, process.stdout)

function closeServer (server) {
  return once(server.close() || server, 'close')
}

function heredoc (literals, ...values) {
  const str =
    literals.length > 1
      ? values.reduce((accum, value, idx) => accum + value + literals[idx + 1], literals[0])
      : literals[0]
  const lines = str.trimRight().split(/^/m)
  if (lines.length < 2) return str
  if (lines[0] === '\n') lines.shift()
  const indentRx = /^ +/
  const indentSize = Math.min(...lines.filter((l) => l.startsWith(' ')).map((l) => l.match(indentRx)[0].length))
  return (indentSize ? lines.map((l) => (l.startsWith(' ') ? l.slice(indentSize) : l)) : lines).join('')
}

function sliceObject (obj, ...keys) {
  const result = {}
  for (const k of keys) result[k] = obj[k]
  return result
}

function startGitServer (dir) {
  return new Promise((resolve, reject) => {
    const gitServer = new GitServer(dir, { autoCreate: false })
    gitServer.listen(0, { type: 'http' }, function (err) {
      err ? reject(err) : resolve([gitServer, this.address().port])
    })
  })
}

function trapAsyncError (fn) {
  return fn().then(
    (retVal) => () => retVal,
    (err) => () => {
      throw err
    }
  )
}

async function updateYamlFile (filepath, data) {
  const parsed = yaml.load(await fsp.readFile(filepath), { schema: yaml.CORE_SCHEMA })
  Object.assign(parsed, data)
  await fsp.writeFile(filepath, yaml.dump(parsed, { forceQuotes: true, noArrayIndent: true }), 'utf8')
}

module.exports = {
  after,
  afterEach,
  assert,
  assertx,
  before,
  beforeEach,
  captureStderr,
  captureStdout,
  closeServer,
  describe,
  heredoc,
  it,
  updateYamlFile,
  startGitServer,
  trapAsyncError,
  windows: process.platform === 'win32',
  windowsBatchRequiresShell:
    process.platform === 'win32' && spawnSync('npm.cmd', ['-v'], { windowsHide: true }).error?.code === 'EINVAL',
}
