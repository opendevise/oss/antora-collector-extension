/* eslint-env mocha */
'use strict'

const {
  after,
  afterEach,
  assert,
  assertx,
  before,
  beforeEach,
  captureStderr,
  captureStdout,
  closeServer,
  describe,
  heredoc,
  it,
  startGitServer,
  trapAsyncError,
  updateYamlFile,
  windows,
  windowsBatchRequiresShell,
} = require('@antora/collector-test-harness')
const aggregateContent = require('@antora/content-aggregator')
const { createHash } = require('node:crypto')
const fs = require('node:fs')
const { promises: fsp } = fs
const { isAsyncFunction: isAsync } = require('node:util').types
const getUserCacheDir = require('cache-directory')
const git = require('@antora/content-aggregator/git')
const ospath = require('node:path')

const FIXTURES_DIR = ospath.join(__dirname, 'fixtures')
const WORK_DIR = ospath.join(__dirname, 'work')
const CACHE_DIR = ospath.join(WORK_DIR, 'cache')
const REPOS_DIR = ospath.join(WORK_DIR, 'repos')

describe('collector extension', () => {
  describe('bootstrap', () => {
    it('should be able to require extension', () => {
      const exports = require('@antora/collector-extension')
      assert(exports instanceof Object)
      assert(exports.register instanceof Function)
    })
  })

  describe('integration', () => {
    const ext = require('@antora/collector-extension')

    let gitServer
    let gitServerPort
    let messages

    const cleanWorkDir = async (opts = {}) => {
      await fsp.rm(WORK_DIR, { recursive: true, force: true })
      if (opts.create) await fsp.mkdir(WORK_DIR, { recursive: true })
    }

    const getCollectorCacheDir = () => {
      return ospath.join(CACHE_DIR, 'collector')
    }

    const getCollectorWorktree = (origin) => {
      let folderName
      if (origin.worktree === undefined) {
        folderName = ospath.basename(origin.gitdir, '.git')
      } else {
        const url = origin.url.toLowerCase()
        folderName = `${url.slice(url.lastIndexOf('/') + 1)}-${createHash('sha1').update(url).digest('hex')}`
      }
      return ospath.join(getCollectorCacheDir(), folderName)
    }

    before(async () => {
      await cleanWorkDir({ create: true })
      ;[gitServer, gitServerPort] = await startGitServer(REPOS_DIR)
    })

    after(async () => {
      await closeServer(gitServer.server)
      await fsp.rm(WORK_DIR, { recursive: true, force: true })
    })

    beforeEach(async () => {
      messages = []
    })

    afterEach(async () => {
      await cleanWorkDir()
    })

    const createGeneratorContext = () => ({
      require: () => git,
      once (eventName, fn) {
        this[eventName] = fn
      },
      getLogger: (name) =>
        new Proxy(
          {},
          {
            get (target, property) {
              if (property === 'isLevelEnabled') return () => true
              return (msg, err) => {
                if (msg instanceof Error) {
                  err = msg
                  msg = undefined
                }
                messages.push({ name, level: property, msg, err })
              }
            },
          }
        ),
    })

    const createRepository = async ({ repoName, fixture = repoName, branches, tags, startPath, collectorConfig }) => {
      const repo = { dir: ospath.join(REPOS_DIR, repoName), fs }
      const links = []
      const captureLinks = function (src, dest) {
        if (!src.endsWith('-link')) return true
        return fsp.readFile(src, 'utf8').then((link) => {
          const [from, to] = link.trim().split(' -> ')
          this.push([ospath.join(ospath.dirname(dest), from), to])
          return false
        })
      }
      await fsp.cp(ospath.join(FIXTURES_DIR, fixture), repo.dir, { recursive: true, filter: captureLinks.bind(links) })
      for (const [from, to] of links) {
        const type = to.endsWith('/') ? 'dir' : 'file'
        await fsp.symlink(type === 'dir' ? to.slice(0, -1) : to, from, type)
      }
      if (collectorConfig) {
        const antoraYmlPath = ospath.join(repo.dir, startPath || '', 'antora.yml')
        await updateYamlFile(antoraYmlPath, { ext: { collector: collectorConfig } })
      }
      await git.init({ ...repo, defaultBranch: 'main' })
      await git
        .statusMatrix(repo)
        .then((status) =>
          Promise.all(
            status.map(([filepath, _, worktreeStatus]) =>
              worktreeStatus === 0 ? git.remove({ ...repo, filepath }) : git.add({ ...repo, filepath })
            )
          )
        )
      await git.commit({ ...repo, author: { name: 'Tester', email: 'tester@example.org' }, message: 'initial import' })
      if (branches) {
        for (const branch of branches) await git.branch({ ...repo, ref: branch })
      }
      if (tags) {
        for (const tag of tags) await git.tag({ ...repo, ref: tag })
      }
      repo.url = `http://localhost:${gitServerPort}/${repoName}/.git`
      return repo
    }

    const runScenario = async (opts) => {
      const { repoName, branches, tags, startPath, local, collectorConfig, quiet, before, after } = opts
      const repo = await createRepository({ repoName, branches, tags, startPath, collectorConfig })
      const playbook = {
        content: {
          sources: [
            {
              url: local ? repo.dir : repo.url,
              startPath,
              branches: branches || 'main',
              tags,
            },
          ],
        },
        env: process.env,
        runtime: { cacheDir: CACHE_DIR, quiet: quiet == null ? true : quiet },
      }
      const contentAggregate = await aggregateContent(playbook)
      if (before) isAsync(before) ? await before(contentAggregate, playbook) : before(contentAggregate, playbook)
      const generatorContext = createGeneratorContext()
      const registerVars = { config: collectorConfig?.globalConfig || {} }
      ext.register.call(generatorContext, registerVars)
      await generatorContext.contentAggregated({ playbook, contentAggregate })
      if (after) isAsync(after) ? await after(contentAggregate) : after(contentAggregate)
      if (typeof generatorContext.contextClosed === 'function') await generatorContext.contextClosed()
    }

    it('should not allocate worktree for reference in git tree if collector is not configured', async () => {
      await runScenario({
        repoName: 'at-root',
        before: async (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assertx.empty(bucket.files)
          const worktree = getCollectorWorktree(bucket.origins[0])
          await fsp.mkdir(ospath.dirname(worktree), { recursive: true })
          await fsp.writeFile(worktree, Buffer.alloc(0))
        },
        after: (contentAggregate) => {
          assertx.empty(contentAggregate[0].files)
        },
      })
    })

    it('should not run command if value is empty', async () => {
      const collectorConfig = {
        run: { command: '' },
        scan: { dir: '.' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assertx.empty(contentAggregate[0].files)
        },
      })
    })

    it('should not run command if value is not a string', async () => {
      const collectorConfig = {
        run: { command: true },
        scan: { dir: '.' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assertx.empty(contentAggregate[0].files)
        },
      })
    })

    it('should run specified command in temporary worktree and collect files in scan dir', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert.equal(contentAggregate[0].files[0].path, 'modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should log command at info level if log level enabled', async () => {
      const collectorConfig = { run: { command: '$NODE .gen-start-page.js' } }
      const expectedMessage = `run.command: "${process.execPath}" .gen-start-page.js (url: http://localhost:${gitServerPort}/at-root/.git | branch: main)`
      await runScenario({ repoName: 'at-root', collectorConfig, quiet: false })
      assert.equal(messages.length, 1)
      const message = messages[0]
      const expectedMessageObj = {
        name: '@antora/collector-extension',
        level: 'info',
        msg: expectedMessage,
      }
      assertx.deepEqualSlice(message, expectedMessageObj)
    })

    it('should keep temporary worktree if specified', async () => {
      const collectorConfig = {
        globalConfig: { keepWorktrees: true },
        run: { command: '$NODE .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      let collectorWorktree
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          collectorWorktree = contentAggregate[0].files[0].src.origin.collectorWorktree
          assertx.directory(collectorWorktree)
          assert(ospath.basename(collectorWorktree).includes('@main'))
        },
      })
      assertx.directory(collectorWorktree)
      await fsp.rm(collectorWorktree, { recursive: true })
    })

    it('should keep temporary worktree until specified event', async () => {
      const collectorConfig = {
        globalConfig: { keepWorktrees: 'until:contextClosed' },
        run: { command: '$NODE .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      let collectorWorktree
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          collectorWorktree = contentAggregate[0].files[0].src.origin.collectorWorktree
          assertx.directory(collectorWorktree)
          assert(ospath.basename(collectorWorktree).includes('@main'))
        },
      })
      assert(!fs.existsSync(collectorWorktree))
    })

    it('should keep temporary worktree until specified event when specified on collector config', async () => {
      const collectorConfig = {
        worktree: { keep: 'until:exit' },
        run: { command: '$NODE .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      let collectorWorktree
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          collectorWorktree = contentAggregate[0].files[0].src.origin.collectorWorktree
          assertx.directory(collectorWorktree)
          assert(ospath.basename(collectorWorktree).includes('@main'))
        },
      })
      assert(!fs.existsSync(collectorWorktree))
    })

    it('should populate properties of file collected from temporary worktree', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          const bucket = contentAggregate[0]
          const files = bucket.files
          const expectedScanned = 'build/modules/ROOT/pages/index.adoc'
          const expectedRealpath = ospath.join(getCollectorWorktree(bucket.origins[0]), expectedScanned)
          assert.equal(files.length, 1)
          assert('stat' in files[0])
          const expectedSrc = {
            path: 'modules/ROOT/pages/index.adoc',
            basename: 'index.adoc',
            stem: 'index',
            extname: '.adoc',
            abspath: expectedScanned,
            realpath: expectedRealpath,
            scanned: expectedScanned,
            origin: contentAggregate[0].origins[0],
          }
          assertx.deepEqualSlice(files[0].src, expectedSrc)
        },
      })
    })

    it('should populate properties of file collected from start path in temporary worktree', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-docs-start-page.js' },
        scan: { dir: 'build/docs' },
      }
      await runScenario({
        repoName: 'at-start-path',
        startPath: 'docs',
        collectorConfig,
        after: (contentAggregate) => {
          const bucket = contentAggregate[0]
          const files = bucket.files
          const expectedScanned = 'build/docs/modules/ROOT/pages/index.adoc'
          const expectedRealpath = ospath.join(getCollectorWorktree(bucket.origins[0]), expectedScanned)
          assert.equal(files.length, 1)
          assert.equal(files[0].path, 'modules/ROOT/pages/index.adoc')
          const expectedSrc = {
            path: 'modules/ROOT/pages/index.adoc',
            basename: 'index.adoc',
            stem: 'index',
            extname: '.adoc',
            abspath: expectedScanned,
            realpath: expectedRealpath,
            scanned: expectedScanned,
            origin: contentAggregate[0].origins[0],
          }
          assertx.deepEqualSlice(files[0].src, expectedSrc)
        },
      })
    })

    it('should populate properties of file collected from local worktree', async () => {
      const collectorConfig = {
        clean: { dir: 'build' },
        run: { command: '$NODE .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        local: true,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          const expectedScanned = 'build/modules/ROOT/pages/index.adoc'
          const expectedAbspath = ospath.join(contentAggregate[0].origins[0].worktree, expectedScanned)
          const collectedFile = contentAggregate[0].files[0]
          assert('stat' in collectedFile)
          const expectedSrc = {
            path: 'modules/ROOT/pages/index.adoc',
            basename: 'index.adoc',
            stem: 'index',
            extname: '.adoc',
            abspath: expectedAbspath,
            scanned: expectedScanned,
            origin: contentAggregate[0].origins[0],
          }
          assertx.deepEqualSlice(collectedFile.src, expectedSrc)
          assert.equal(collectedFile.src.origin.collectorWorktree, collectedFile.src.origin.worktree)
        },
      })
    })

    it('should not run in local worktree if createWorktrees is set to always on extension', async () => {
      const collectorConfig = {
        globalConfig: { createWorktrees: 'always' },
        run: { command: 'node .gen-start-page.js', create: null },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        local: true,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          const expectedScanned = 'build/modules/ROOT/pages/index.adoc'
          const localAbspath = ospath.join(contentAggregate[0].origins[0].worktree, expectedScanned)
          assert('stat' in contentAggregate[0].files[0])
          const expectedSrc = {
            path: 'modules/ROOT/pages/index.adoc',
            basename: 'index.adoc',
            stem: 'index',
            extname: '.adoc',
            abspath: expectedScanned,
            scanned: expectedScanned,
            origin: contentAggregate[0].origins[0],
          }
          assertx.deepEqualSlice(contentAggregate[0].files[0].src, expectedSrc)
          assert.notEqual(contentAggregate[0].files[0].src.abspath, localAbspath)
        },
      })
    })

    it('should not run in local worktree if worktree.create is set to always on collector config', async () => {
      const collectorConfig = {
        worktree: { create: 'always' },
        run: { command: 'node .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        local: true,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          const expectedScanned = 'build/modules/ROOT/pages/index.adoc'
          const localAbspath = ospath.join(contentAggregate[0].origins[0].worktree, expectedScanned)
          assert('stat' in contentAggregate[0].files[0])
          const expectedSrc = {
            path: 'modules/ROOT/pages/index.adoc',
            basename: 'index.adoc',
            stem: 'index',
            extname: '.adoc',
            abspath: expectedScanned,
            scanned: expectedScanned,
            origin: contentAggregate[0].origins[0],
          }
          assertx.deepEqualSlice(contentAggregate[0].files[0].src, expectedSrc)
          assert.notEqual(contentAggregate[0].files[0].src.abspath, localAbspath)
        },
      })
    })

    it('should not run in local worktree if worktree.checkout is set to always on collector config', async () => {
      const collectorConfig = {
        worktree: { checkout: 'always', create: 'ignored' },
        run: { command: 'node .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        local: true,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          const expectedScanned = 'build/modules/ROOT/pages/index.adoc'
          const localAbspath = ospath.join(contentAggregate[0].origins[0].worktree, expectedScanned)
          assert('stat' in contentAggregate[0].files[0])
          const expectedSrc = {
            path: 'modules/ROOT/pages/index.adoc',
            basename: 'index.adoc',
            stem: 'index',
            extname: '.adoc',
            abspath: expectedScanned,
            scanned: expectedScanned,
            origin: contentAggregate[0].origins[0],
          }
          assertx.deepEqualSlice(contentAggregate[0].files[0].src, expectedSrc)
          assert.notEqual(contentAggregate[0].files[0].src.abspath, localAbspath)
        },
      })
    })

    it('should not run in local worktree if worktree is false on collector config', async () => {
      const collectorConfig = {
        worktree: false,
        run: { command: 'node .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        local: true,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          const expectedScanned = 'build/modules/ROOT/pages/index.adoc'
          const localAbspath = ospath.join(contentAggregate[0].origins[0].worktree, expectedScanned)
          assert('stat' in contentAggregate[0].files[0])
          const expectedSrc = {
            path: 'modules/ROOT/pages/index.adoc',
            basename: 'index.adoc',
            stem: 'index',
            extname: '.adoc',
            abspath: expectedScanned,
            scanned: expectedScanned,
            origin: contentAggregate[0].origins[0],
          }
          assertx.deepEqualSlice(contentAggregate[0].files[0].src, expectedSrc)
          assert.notEqual(contentAggregate[0].files[0].src.abspath, localAbspath)
        },
      })
    })

    it('should update component metadata from antora.yml file, if found', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-component-desc.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.version, 'main')
          assertx.empty(bucket.files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.version, '1.0.0')
          assert.equal(bucket.title, 'Test')
          assertx.empty(bucket.files)
          assert.equal(bucket.asciidoc.attributes['url-api'], 'https://api.example.org')
          const expectedScriptDirname = getCollectorWorktree(bucket.origins[0])
          assert.equal(bucket.asciidoc.attributes['script-dirname'], expectedScriptDirname)
        },
      })
    })

    it('should remove prerelease key if not specified in scanned antora.yml file', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-component-desc.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root-prerelease',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.version, '')
          assert.equal(bucket.prerelease, true)
          assertx.empty(bucket.files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.version, '1.0.0')
          assert.equal(bucket.prerelease, undefined)
          assertx.empty(bucket.files)
        },
      })
    })

    it('should change name and version of current component version if specified antora.yml', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-component-desc-with-new-identity.js' },
        scan: { dir: 'build' },
      }
      let bucketIdentityBefore
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucketBefore = contentAggregate[0]
          assertx.empty(bucketBefore.files)
          bucketIdentityBefore = { name: bucketBefore.name, version: bucketBefore.version }
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucketAfter = contentAggregate[0]
          assert.equal(bucketAfter.name, 'new-name')
          assert.equal(bucketAfter.version, '1.0')
          assert.equal(bucketAfter.origins[0].descriptor.name, bucketIdentityBefore.name)
          assert.equal(bucketAfter.origins[0].descriptor.version, bucketIdentityBefore.version)
          assertx.empty(bucketAfter.files)
        },
      })
    })

    it('should inherit name and version if not specified antora.yml', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-component-desc-without-identity.js' },
        scan: { dir: 'build' },
      }
      let bucketIdentityBefore
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucketBefore = contentAggregate[0]
          assertx.empty(bucketBefore.files)
          bucketIdentityBefore = { name: bucketBefore.name, version: bucketBefore.version }
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucketAfter = contentAggregate[0]
          assert.equal(bucketAfter.name, bucketIdentityBefore.name)
          assert.equal(bucketAfter.version, bucketIdentityBefore.version)
          assert.equal(bucketAfter.asciidoc.attributes['url-api'], 'https://api.example.org')
          assertx.empty(bucketAfter.files)
        },
      })
    })

    it('should only update contents of existing page', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-replace-start-page.js' },
        scan: { dir: 'build' },
      }
      let originalStat
      await runScenario({
        repoName: 'replace',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assert.equal(contentAggregate[0].files.length, 1)
          assert(contentAggregate[0].files[0].contents.includes('= Stub Start Page'))
          originalStat = contentAggregate[0].files[0].stat
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          const expectedContents = heredoc`
            = Real Start Page

            This is the real deal.
          `
          assert.equal(contentAggregate[0].files[0].contents.toString(), expectedContents + '\n')
          assert.notEqual(contentAggregate[0].files[0].stat, originalStat)
          assert.notEqual(contentAggregate[0].files[0].stat.size, originalStat.size)
          assert(!('scanned' in contentAggregate[0].files[0].src))
        },
      })
    })

    it('should run specified command in start path of temporary worktree and collect files in scan dir', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-start-page.js', dir: 'docs' },
        scan: { dir: './build' },
      }
      await runScenario({
        repoName: 'at-start-path',
        startPath: 'docs',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert.equal(contentAggregate[0].files[0].path, 'modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should resolve env.PWD to dir specified on run entry', async () => {
      const collectorConfig = {
        run: { command: '$NODE "$' + '{{env.PWD}}/.gen-start-page.js"', dir: 'docs' },
        scan: { dir: './build' },
      }
      await runScenario({
        repoName: 'at-start-path',
        startPath: 'docs',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert.equal(contentAggregate[0].files[0].path, 'modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should run specified command from root of temporary worktree and collect files in scan dir', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-docs-start-page.js' },
        scan: { dir: 'build/docs' },
      }
      await runScenario({
        repoName: 'at-start-path',
        startPath: 'docs',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert.equal(contentAggregate[0].files[0].path, 'modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should prepend node exec path to command identified as a JavaScript file', async () => {
      const collectorConfig = {
        run: { command: '.gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert.equal(contentAggregate[0].files[0].path, 'modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should run specified local command if command begins with ./', async () => {
      const collectorConfig = {
        run: { command: './.gen-start-page' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert.equal(contentAggregate[0].files[0].path, 'modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should run specified local command if local option is true', async () => {
      const collectorConfig = {
        run: { command: '.gen-start-page', local: true },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert.equal(contentAggregate[0].files[0].path, 'modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should run specified local command in parent directory', async () => {
      const collectorConfig = {
        run: { command: '../.gen-docs-start-page', dir: '.' },
        scan: { dir: 'build/docs' },
      }
      await runScenario({
        repoName: 'at-start-path',
        startPath: 'docs',
        collectorConfig,
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert.equal(contentAggregate[0].files[0].path, 'modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should run specified command with absolute path even if local option is true', async () => {
      const collectorConfig = {
        run: { command: '$' + '{{origin.collectorWorktree}}/.gen-start-page', local: true },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert.equal(contentAggregate[0].files[0].path, 'modules/ROOT/pages/index.adoc')
        },
      })
    })

    // NOTE that command must be independently quoted from quoted string in YAML
    it('should support local command with spaces enclosed in quotes', async () => {
      const collectorConfig = {
        // NOTE test both variations
        run: [{ command: './".gen start page"' }, { command: '".gen start page"', local: true }],
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert.equal(contentAggregate[0].files[0].path, 'modules/ROOT/pages/index.adoc')
        },
      })
    })

    if (!windows) {
      it('should resolve $NPM at start of command', async () => {
        const collectorConfig = {
          run: { command: '$NPM run gen-docs-start-page' },
          scan: { dir: 'build/docs' },
        }
        await runScenario({
          repoName: 'at-start-path',
          startPath: 'docs',
          collectorConfig,
          after: (contentAggregate) => {
            const bucket = contentAggregate[0]
            const files = bucket.files
            assert.equal(files.length, 1)
            assert.equal(files[0].path, 'modules/ROOT/pages/index.adoc')
          },
        })
      })
    }

    if (windows) {
      it('should run local batch script on Windows', async () => {
        await runScenario({
          repoName: 'batch-script',
          startPath: 'docs',
          after: (contentAggregate) => {
            assert.equal(contentAggregate.length, 1)
            const bucket = contentAggregate[0]
            assert.equal(bucket.name, 'test')
            assert.equal(bucket.version, '1.0')
          },
        })
      })

      it('should run batch script on PATH in Windows', async () => {
        const collectorConfig = {
          run: [{ command: 'npm run' }, { command: 'npm run gen-antora-yml' }],
          scan: { dir: 'target/docs' },
        }
        await runScenario({
          repoName: 'batch-script',
          startPath: 'docs',
          collectorConfig,
          after: (contentAggregate) => {
            assert.equal(contentAggregate.length, 1)
            const bucket = contentAggregate[0]
            assert.equal(bucket.name, 'test')
            assert.equal(bucket.version, '1.0')
          },
        })
      })
    }

    if (windowsBatchRequiresShell) {
      it('should revert to shell with escaped arguments if command is batch script', async () => {
        const collectorConfig = {
          run: [{ command: '.gen-file-from-arg.bat "<foo>&bar b^z\\""', local: true }],
          scan: { dir: 'build' },
        }
        await runScenario({
          repoName: 'at-root',
          collectorConfig,
          after: (contentAggregate) => {
            assert.equal(contentAggregate.length, 1)
            assert.equal(contentAggregate[0].files.length, 1)
            assert.equal(contentAggregate[0].files[0].path, 'modules/ROOT/pages/index.adoc')
            assert(contentAggregate[0].files[0].contents.includes('"<"foo">&"bar" "b"^"z"""'))
          },
        })
      })
    }

    it('should run specified command from content root if dir option is .', async () => {
      const collectorConfig = {
        run: { command: '.gen-start-page', local: true, dir: '.' },
        scan: { dir: './build' },
      }
      await runScenario({
        repoName: 'at-start-path',
        startPath: 'docs',
        collectorConfig,
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert.equal(contentAggregate[0].files[0].path, 'modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should not collect files if scan key is not specified', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-start-page.js' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assertx.empty(contentAggregate[0].files)
        },
      })
    })

    it('should not collect files if scan dir key is not a string', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-start-page.js' },
        scan: {},
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assertx.empty(contentAggregate[0].files)
        },
      })
    })

    it('should collect files without running command if only scan key is specified', async () => {
      const collectorConfig = {
        scan: { dir: 'other-docs' },
      }
      await runScenario({
        repoName: 'at-start-path',
        startPath: 'docs',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert.equal(contentAggregate[0].files[0].path, 'modules/ROOT/pages/outside-start-path.adoc')
        },
      })
    })

    it('should not fail if scan dir does not exist', async () => {
      const collectorConfig = {
        scan: { dir: 'does-not-exist' },
      }
      await runScenario({
        repoName: 'at-start-path',
        startPath: 'docs',
        collectorConfig,
        local: true,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assertx.empty(contentAggregate[0].files)
        },
      })
    })

    it('should not delete files in scan dir of worktree before running scan', async () => {
      const collectorConfig = {
        scan: { dir: 'other-docs' },
      }
      await runScenario({
        repoName: 'at-start-path',
        startPath: 'docs',
        collectorConfig,
        local: true,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert.equal(contentAggregate[0].files[0].path, 'modules/ROOT/pages/outside-start-path.adoc')
        },
      })
    })

    it('should rebase files if into key is set on scan', async () => {
      const collectorConfig = {
        scan: { dir: 'code', into: 'modules/extend/examples' },
      }
      await runScenario({
        repoName: 'at-start-path',
        startPath: 'docs',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert.equal(contentAggregate[0].files[0].path, 'modules/extend/examples/extended-pdf-converter.rb')
        },
      })
    })

    it('should drop leading / on value of into key', async () => {
      const collectorConfig = {
        scan: { dir: 'code', into: '/modules/extend/examples' },
      }
      await runScenario({
        repoName: 'at-start-path',
        startPath: 'docs',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert.equal(contentAggregate[0].files[0].path, 'modules/extend/examples/extended-pdf-converter.rb')
        },
      })
    })

    it('should put files into bucket specified on scan', async () => {
      const collectorConfig = {
        scan: { dir: 'code', into: { name: 'shared', version: '', dir: 'modules/ROOT/examples' } },
      }
      await runScenario({
        repoName: 'at-start-path',
        startPath: 'docs',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 2)
          assert.equal(contentAggregate[1].files.length, 1)
          assert.equal(contentAggregate[1].name, 'shared')
          assert.equal(contentAggregate[1].version, '')
          assert.equal(contentAggregate[1].files[0].path, 'modules/ROOT/examples/extended-pdf-converter.rb')
        },
      })
    })

    it('should put files into existing bucket specified in antora.yml', async () => {
      const collectorConfig = {
        run: [{ command: '$NODE .gen-component-desc.js create' }, { command: '$NODE .gen-more-files.js' }],
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.version, 'main')
          assertx.empty(bucket.files)
          contentAggregate.push({
            name: 'test',
            version: '1.0.0',
            files: [],
            origins: [],
          })
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 2)
          const bucket = contentAggregate[1]
          assert.equal(bucket.files.length, 1)
          assert.equal(bucket.name, 'test')
          assert.equal(bucket.version, '1.0.0')
          assert.equal(bucket.files[0].path, 'modules/more/pages/index.adoc')
        },
      })
    })

    it('should put files into new bucket specified in antora.yml', async () => {
      const collectorConfig = {
        run: [{ command: '$NODE .gen-component-desc.js create' }, { command: '$NODE .gen-more-files.js' }],
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.version, 'main')
          assertx.empty(bucket.files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 2)
          const bucket = contentAggregate[1]
          assert.equal(bucket.files.length, 1)
          assert.equal(bucket.name, 'test')
          assert.equal(bucket.version, '1.0.0')
          assert.equal(bucket.files[0].path, 'modules/more/pages/index.adoc')
        },
      })
    })

    it('should allow bucket in antora.yml to override bucket specified on scan', async () => {
      const collectorConfig = {
        run: [{ command: '$NODE .gen-component-desc.js' }, { command: '$NODE .gen-more-files.js' }],
        scan: { dir: 'build', into: { name: 'overridden-name', version: 'overridden-version' } },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.version, 'main')
          assertx.empty(bucket.files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.files.length, 1)
          assert.equal(bucket.name, 'test')
          assert.equal(bucket.version, '1.0.0')
          assert.equal(bucket.files[0].path, 'modules/more/pages/index.adoc')
        },
      })
    })

    it('should collect files from multiple scan dirs', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-start-page.js', dir: '.' },
        scan: [{ dir: './build' }, { dir: 'other-docs' }],
      }
      await runScenario({
        repoName: 'at-start-path',
        startPath: 'docs',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 2)
          assert.deepEqual(contentAggregate[0].files.map((it) => it.path).sort(), [
            'modules/ROOT/pages/index.adoc',
            'modules/ROOT/pages/outside-start-path.adoc',
          ])
        },
      })
    })

    it('should collect more than 16 files from single directory when scanning', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-many-files.js 20', dir: '.' },
        scan: { dir: './build/many-pages', files: '*.adoc', into: 'modules/ROOT/pages' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 20)
          const expected = []
          for (let i = 1; i <= 20; i++) expected.push(`modules/ROOT/pages/${i < 10 ? '0' : ''}${i}.adoc`)
          assert.deepEqual(contentAggregate[0].files.map((it) => it.path).sort(), expected)
        },
      })
    })

    it('should process each collector entry', async () => {
      const collectorConfig = [
        {
          run: { command: '$NODE .gen-files.js' },
          scan: { dir: 'build' },
        },
        {
          run: { command: '$NODE .gen-component-desc.js' },
          scan: { dir: 'build', files: 'antora.yml' },
        },
      ]
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assert.equal(contentAggregate[0].version, 'main')
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.version, '1.0.0')
          assert.equal(bucket.files.length, 3)
          assert.deepEqual(bucket.files.map((it) => it.path).sort(), [
            'modules/ROOT/examples/ack.rb',
            'modules/ROOT/pages/index.adoc',
            'modules/ROOT/partials/summary.adoc',
          ])
          assert(
            contentAggregate[0].files
              .find((it) => it.path === 'modules/ROOT/pages/index.adoc')
              .contents.includes('= The Start Page')
          )
        },
      })
    })

    it('should not clean worktree between collector entries', async () => {
      const collectorConfig = [
        {
          run: [{ command: '$NODE .gen-files.js' }, { command: '$NODE .gen-component-desc.js' }],
        },
        {
          run: { command: '$NODE .gen-start-page.js' },
          scan: [
            { dir: 'build', files: 'antora.yml' },
            { dir: 'build', files: 'modules/**/*' },
          ],
        },
      ]
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assert.equal(contentAggregate[0].version, 'main')
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.version, '1.0.0')
          assert.equal(bucket.files.length, 3)
          assert.deepEqual(bucket.files.map((it) => it.path).sort(), [
            'modules/ROOT/examples/ack.rb',
            'modules/ROOT/pages/index.adoc',
            'modules/ROOT/partials/summary.adoc',
          ])
          assert(
            contentAggregate[0].files
              .find((it) => it.path === 'modules/ROOT/pages/index.adoc')
              .contents.includes('= Start Page')
          )
        },
      })
    })

    it('should clean specified dirs per collector entry', async () => {
      const collectorConfig = [
        {
          run: [{ command: '$NODE .gen-files.js' }, { command: '$NODE .gen-component-desc.js' }],
        },
        {
          clean: [{ dir: 'build/modules/ROOT/examples' }, { dir: 'build/modules/ROOT/partials' }],
          run: { command: '$NODE .gen-start-page.js' },
          scan: [
            { dir: 'build', files: 'antora.yml' },
            { dir: 'build', files: 'modules/**/*' },
          ],
        },
      ]
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assert.equal(contentAggregate[0].version, 'main')
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.version, '1.0.0')
          assert.equal(bucket.files.length, 1)
          assert.deepEqual(bucket.files.map((it) => it.path).sort(), ['modules/ROOT/pages/index.adoc'])
          assert(
            contentAggregate[0].files
              .find((it) => it.path === 'modules/ROOT/pages/index.adoc')
              .contents.includes('= Start Page')
          )
        },
      })
    })

    it('should support short form value for clean, run, and scan', async () => {
      const collectorConfig = [
        {
          run: ['node .gen-files.js', 'node .gen-component-desc.js'],
        },
        {
          clean: 'build/modules',
          run: 'node .gen-start-page.js',
          scan: 'build',
        },
      ]
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.version, '1.0.0')
          assert.equal(bucket.files.length, 1)
          assert.deepEqual(bucket.files.map((it) => it.path).sort(), ['modules/ROOT/pages/index.adoc'])
        },
      })
    })

    it('should only collect files that match the scan pattern', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-files.js' },
        scan: { dir: 'build', files: '**/*.adoc' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 2)
          const paths = contentAggregate[0].files.map((it) => it.path).sort()
          assert.deepEqual(paths, ['modules/ROOT/pages/index.adoc', 'modules/ROOT/partials/summary.adoc'])
        },
      })
    })

    it('should support an array of scan patterns with inclusions and exclusions', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-files.js' },
        scan: { dir: 'build', files: ['**/*.adoc', '!modules/*/partials/**/*.adoc', 'modules/**/*.rb'] },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 2)
          const paths = contentAggregate[0].files.map((it) => it.path).sort()
          assert.deepEqual(paths, ['modules/ROOT/examples/ack.rb', 'modules/ROOT/pages/index.adoc'])
        },
      })
    })

    it('should set specified environment variables for run operation', async () => {
      const collectorConfig = {
        run: [
          { command: '$NODE .gen-component-desc.js' },
          {
            command: '$NODE .gen-using-env.js',
            env: [
              {
                name: 'ANTORA_PLAYBOOK',
                value: '$' + '{{ playbook as json }}',
              },
              {
                name: 'ANTORA_COLLECTOR_ORIGIN',
                value: '$' + '{{ origin as json }}',
              },
              {
                name: 'ANTORA_COLLECTOR_WORKTREE',
                value: '$' + '{{ origin.collectorWorktree }}',
              },
              {
                name: 'ANTORA_COLLECTOR_SITE_TITLE',
                value: '$' + '{{ playbook.site.title }} ($' + '{{ env.NODE_ENV }})',
              },
            ],
          },
        ],
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate, playbook) => {
          playbook.site = { title: 'My Site', url: 'https://docs.example.org' }
          Object.freeze(playbook)
          assert.equal(contentAggregate.length, 1)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          const expectedWorktree = getCollectorWorktree(bucket.origins[0])
          const expectedContents = heredoc`
            reftype:: branch
            refname:: main
            worktree:: ${expectedWorktree}
            site title:: My Site (test)
            site url:: https://docs.example.org
            env in playbook:: false
          `
          assert.equal(bucket.files.length, 1)
          assert.equal(bucket.files[0].path, 'modules/ROOT/pages/index.adoc')
          assert(bucket.files[0].contents.includes(expectedContents))
        },
      })
    })

    it('should handle non-string environment variable values', async () => {
      const collectorConfig = {
        run: [
          {
            command: '$NODE .gen-component-desc.js',
            env: null,
          },
          {
            command: '$NODE .gen-using-env.js',
            env: [{ name: 'USER', value: null }, { name: 'ENV_VALUE_AS_STRING', value: 99.9 }, null],
          },
        ],
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate, playbook) => {
          playbook.site = { title: 'My Site' }
          assert.equal(contentAggregate.length, 1)
        },
        after: (contentAggregate) => {
          const expectedContents = '{"ENV_VALUE_AS_STRING":"99.9"}'
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.files.length, 1)
          assert.equal(bucket.files[0].path, 'modules/ROOT/pages/index.adoc')
          assert(bucket.files[0].contents.includes(expectedContents))
        },
      })
    })

    it('should allow value of env key on run to be a map', async () => {
      const collectorConfig = {
        run: [
          { command: '$NODE .gen-component-desc.js' },
          {
            command: '$NODE .gen-using-env.js',
            env: { refname: '$' + '{{origin.refname}}' },
          },
        ],
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate, playbook) => {
          assert.equal(contentAggregate.length, 1)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.files.length, 1)
          assert.equal(bucket.files[0].path, 'modules/ROOT/pages/index.adoc')
          assert.equal(bucket.files[0].contents.toString(), '= Refname\n\nmain')
        },
      })
    })

    it('should read environment variables from playbook', async () => {
      const collectorConfig = {
        run: {
          command: 'node .gen-many-files.js $' + '{{env.NUM_FILES}}',
          dir: '.',
        },
        scan: { dir: './build/many-pages', files: '*.adoc', into: 'modules/ROOT/pages' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate, playbook) => {
          playbook.env = Object.assign({}, playbook.env, { NUM_FILES: '5' })
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 5)
        },
      })
    })

    it('should allow context variables to be referenced in command', async () => {
      const collectorConfig = {
        run: {
          command: '$NODE .gen-many-files.js $' + '{{env.NUM_FILES}}',
          dir: '.',
          env: [{ name: 'NUM_FILES', value: '5' }],
        },
        scan: { dir: './build/many-pages', files: '*.adoc', into: 'modules/ROOT/pages' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 5)
        },
      })
    })

    it('should allow command to be run in default shell', async () => {
      const collectorConfig = {
        run: { command: '.gen-start-page.js', shell: true },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert.equal(contentAggregate[0].files[0].path, 'modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should allow command to be run in default shell and reference environment variables', async () => {
      const collectorConfig = {
        run: {
          command: '"$NODE" "$PWD/.gen-many-files.js" $' + 'NUM_FILES',
          shell: true,
          dir: '.',
          env: [{ name: 'NUM_FILES', value: '5' }],
        },
        scan: { dir: './build/many-pages', files: '*.adoc', into: 'modules/ROOT/pages' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 5)
        },
      })
    })

    // NOTE that command must be independently quoted from quoted string in YAML
    it('should allow local command with spaces enclosed in spaces to be run in default shell', async () => {
      const collectorConfig = {
        run: { command: '".gen start page"', local: true, shell: true },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert.equal(contentAggregate[0].files[0].path, 'modules/ROOT/pages/index.adoc')
        },
      })
    })

    it('should set NODE environment variable even if value of env key has no entries', async () => {
      const node = process.env.NODE
      try {
        delete process.env.NODE
        const collectorConfig = {
          run: { command: './.gen-start-page', dir: '.', env: [] },
          scan: { dir: 'build' },
        }
        await runScenario({
          repoName: 'at-root',
          collectorConfig,
          before: (contentAggregate) => {
            assert.equal(contentAggregate.length, 1)
            assertx.empty(contentAggregate[0].files)
          },
          after: (contentAggregate) => {
            assert.equal(contentAggregate[0].files.length, 1)
          },
        })
      } finally {
        if (node) process.env.NODE = node
      }
    })

    it('should create dedicated cache folder for collector under Antora cache dir', async () => {
      await fsp.mkdir(CACHE_DIR, { recursive: true })
      await fsp.writeFile(getCollectorCacheDir(), Buffer.alloc(0))
      assert.throws(await trapAsyncError(() => runScenario({ repoName: 'at-root' })), {
        message: /file already exists/,
      })
    })

    it('should create cache folder under user cache if cache dir is not specified', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        before: (_, playbook) => {
          delete playbook.runtime.cacheDir
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 1)
          assert(contentAggregate[0].files[0].src.realpath.startsWith(getUserCacheDir('antora-test')))
        },
      })
    })

    it('should empty dedicated cache dir for collector after run', async () => {
      await runScenario({ repoName: 'at-root' })
      assertx.directory(getCollectorCacheDir())
      assertx.empty(fs.readdirSync(getCollectorCacheDir()))
    })

    it('should reuse worktree without cleaning if no clean step is specified', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        local: true,
        collectorConfig,
        before: async (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
          const worktree = contentAggregate[0].origins[0].worktree
          const untrackedPagePath = ospath.join(worktree, 'build/modules/ROOT/pages/untracked.adoc')
          await fsp.mkdir(ospath.dirname(untrackedPagePath), { recursive: true })
          await fsp.writeFile(untrackedPagePath, '= Untracked', 'utf8')
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate[0].files.length, 2)
          const worktree = contentAggregate[0].origins[0].worktree
          assertx.directory(ospath.join(worktree, 'build'))
          assertx.file(ospath.join(worktree, 'build/modules/ROOT/pages/index.adoc'))
          assertx.file(ospath.join(worktree, 'build/modules/ROOT/pages/untracked.adoc'))
        },
      })
    })

    it('should clean scan dir in worktree before running command(s) if clean key on scan entry is true', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-start-page.js' },
        scan: { clean: true, dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        local: true,
        collectorConfig,
        before: async (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.empty(contentAggregate[0].files)
          const worktree = contentAggregate[0].origins[0].worktree
          const residualPagePath = ospath.join(worktree, 'build/modules/ROOT/pages/residual-page.adoc')
          await fsp.mkdir(ospath.dirname(residualPagePath), { recursive: true })
          await fsp.writeFile(residualPagePath, '= Residual Page', 'utf8')
        },
        after: (contentAggregate) => {
          assert(!contentAggregate[0].files.map((it) => it.path).includes('modules/ROOT/pages/residual-page.adoc'))
        },
      })
    })

    it('should run specified command in temporary worktree if repository is local and reference is not worktree', async () => {
      const collectorConfig = {
        clean: { dir: 'build' },
        run: { command: '$NODE .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        local: true,
        branches: ['other'],
        after: (contentAggregate) => {
          const bucket = contentAggregate[0]
          const expectedRealpath = ospath.join(
            getCollectorWorktree(bucket.origins[0]),
            'build/modules/ROOT/pages/index.adoc'
          )
          assert.equal(bucket.files.length, 1)
          assert.equal(bucket.files[0].src.realpath, expectedRealpath)
        },
      })
    })

    it('should run specified command in each branch that defines collector config', async () => {
      const collectorConfig = {
        clean: { dir: 'build' },
        run: { command: '$NODE .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        local: true,
        branches: ['v1.0.x', 'v2.0.x'],
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 2)
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 2)
          contentAggregate.forEach((bucket) => {
            assert(['v1.0.x', 'v2.0.x'].includes(bucket.origins[0].refname))
            const expectedRealpath = ospath.join(
              getCollectorWorktree(bucket.origins[0]),
              'build/modules/ROOT/pages/index.adoc'
            )
            assert.equal(bucket.files.length, 1)
            assert.equal(bucket.files[0].src.realpath, expectedRealpath)
          })
        },
      })
    })

    it('should run specified command in branch that defines collector config when branch name contains segments', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-component-desc-for-ref.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        branches: ['docs/release-line/1.0'],
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assert.equal(contentAggregate[0].version, 'docs-release-line-1.0')
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.version, '1.0')
          assert.equal(bucket.origins[0].refname, 'docs/release-line/1.0')
          assertx.empty(bucket.files)
        },
      })
    })

    it('should run specified command in tag that defines collector config', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-for-tag.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        branches: [],
        tags: ['v1.0.1'],
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assert.equal(contentAggregate[0].origins[0].refname, 'v1.0.1')
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.version, '1.0.1')
          const expectedRealpath = ospath.join(
            getCollectorWorktree(bucket.origins[0]),
            'build/modules/ROOT/pages/v1.0.1-release.adoc'
          )
          assert.equal(bucket.files.length, 1)
          assert.equal(bucket.files[0].src.realpath, expectedRealpath)
        },
      })
    })

    it('should run command inside worktree for both branches and tags in same repository', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-component-desc-for-ref.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        branches: ['v1.0.x'],
        tags: ['v1.0.1'],
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 2)
          assert.deepEqual(contentAggregate.map((it) => it.origins[0].refname).sort(), ['v1.0.1', 'v1.0.x'])
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 2)
          const branchBucket = contentAggregate.find((it) => it.origins[0].reftype === 'branch')
          assert.equal(branchBucket.version, '1.0')
          assert.equal(branchBucket.prerelease, true)
          const tagBucket = contentAggregate.find((it) => it.origins[0].reftype === 'tag')
          assert.equal(tagBucket.version, '1.0.1')
          assert.equal(tagBucket.prerelease, false)
        },
      })
    })

    it('should not modify index of local repository when checking out ref to worktree', async () => {
      const collectorConfig = {
        clean: { dir: 'build' },
        run: { command: '$NODE .gen-start-page.js' },
        scan: { dir: 'build' },
      }
      let expectedIndex
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        local: true,
        branches: ['v1.0.x'],
        before: async (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          expectedIndex = await fsp.readFile(ospath.join(contentAggregate[0].origins[0].gitdir, 'index'), 'utf8')
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          assertx.contents(ospath.join(contentAggregate[0].origins[0].gitdir, 'index'), expectedIndex)
        },
      })
    })

    it('should remove invalid temporary worktree left behind by previous run', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-component-desc.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        branches: ['v1.0.x'],
        before: async (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.version, 'v1.0.x')
          assertx.empty(bucket.files)
          const worktree = getCollectorWorktree(bucket.origins[0])
          await fsp.mkdir(worktree, { recursive: true })
          await fsp.writeFile(ospath.join(worktree, '.git'), 'nuke me')
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.version, '1.0.0')
          assertx.empty(bucket.files)
        },
      })
    })

    it('should not leave behind index file in managed repository', async () => {
      const collectorConfig = {
        run: { command: '$NODE .check-index-file.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        branches: ['v1.0.x'],
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.files.length, 1)
          assert.equal(ospath.basename(bucket.files[0].path), 'does-not-exist.adoc')
        },
      })
    })

    it('should remove untracked changes when switching worktrees for same repository', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-dirty-worktree.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        branches: ['v1.0.x', 'v2.0.x'],
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 2)
          const [bucket1, bucket2] = contentAggregate
          assert.equal(bucket1.version, 'v1.0.x')
          assert.equal(bucket2.version, 'v2.0.x')
        },
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 2)
          const [bucket1, bucket2] = contentAggregate
          assert.equal(bucket1.version, '1.0')
          assert.equal(bucket1.files.length, 1)
          assert.equal(bucket1.files[0].path, 'modules/ROOT/pages/v1.0.x-release.adoc')
          assert.equal(bucket2.version, '2.0')
          assert.equal(bucket2.files.length, 1)
          assert.equal(bucket2.files[0].path, 'modules/ROOT/pages/v2.0.x-release.adoc')
        },
      })
    })

    it('should not follow symlinks when removing untracked files in worktree', async () => {
      const collectorConfig = {
        run: { command: '$NODE .check-dirty-worktree.js' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'with-symlink',
        collectorConfig,
        branches: ['v1.0.x', 'v2.0.x'],
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 2)
          const [bucket1, bucket2] = contentAggregate
          assert.equal(bucket1.files.length, 3)
          assert.equal(bucket1.files.filter((it) => it.path.endsWith('/Hello.java')).length, 2)
          assertx.empty(bucket1.files.filter((it) => it.path.endsWith('/dirty-worktree.adoc')))
          assert.equal(bucket2.files.length, 3)
          assert.equal(bucket2.files.filter((it) => it.path.endsWith('/Hello.java')).length, 2)
          assertx.empty(bucket2.files.filter((it) => it.path.endsWith('/dirty-worktree.adoc')))
        },
      })
    })

    it('should force checkout if worktree has changes when switching worktrees for same repository', async () => {
      const collectorConfig = {
        run: { command: '$NODE .gen-dirty-worktree.js delete' },
        scan: { dir: 'build' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        branches: ['v1.0.x', 'v2.0.x'],
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 2)
          const [bucket1, bucket2] = contentAggregate
          assert.equal(bucket1.version, '1.0')
          assert.equal(bucket1.files.length, 1)
          assert.equal(bucket1.files[0].path, 'modules/ROOT/pages/v1.0.x-release.adoc')
          assert.equal(bucket2.version, '2.0')
          assert.equal(bucket2.files.length, 1)
          assert.equal(bucket2.files[0].path, 'modules/ROOT/pages/v2.0.x-release.adoc')
        },
      })
    })

    it('should not checkout reference if worktree.checkout is false', async () => {
      const command =
        "node -e 'fs.writeFileSync(process.argv[1], String(fs.existsSync(process.argv[2])))' home.adoc .git"
      const collectorConfig = {
        worktree: { checkout: false },
        run: { command },
        scan: { dir: '.', into: 'modules/ROOT/pages' },
      }
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.files.length, 1)
          assert.equal(bucket.files[0].path, 'modules/ROOT/pages/home.adoc')
          assert.equal(bucket.files[0].contents.toString(), 'false')
        },
      })
    })

    it('should only look for checkout config in first entry in config array', async () => {
      const command =
        "node -e 'fs.writeFileSync(process.argv[1], String(fs.existsSync(process.argv[2])))' home.adoc .git"
      const collectorConfig = [
        {
          worktree: { checkout: false },
        },
        {
          worktree: { checkout: true }, // ignored
          run: { command },
          scan: { dir: '.', into: 'modules/ROOT/pages' },
        },
      ]
      await runScenario({
        repoName: 'at-root',
        collectorConfig,
        after: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          const bucket = contentAggregate[0]
          assert.equal(bucket.files.length, 1)
          assert.equal(bucket.files[0].path, 'modules/ROOT/pages/home.adoc')
          assert.equal(bucket.files[0].contents.toString(), 'false')
        },
      })
    })

    it('should send output from command to stdout by default', async () => {
      const collectorConfig = { run: { command: '$NODE .gen-output.js' } }
      const stdout = await captureStdout(() =>
        runScenario({
          repoName: 'at-root',
          collectorConfig,
          before: (_, playbook) => {
            delete playbook.runtime.quiet
          },
        })
      )
      assert.equal(stdout, 'start\nall done!\n')
    })

    // TODO: perhaps direct these messages to the log?
    it('should always direct stderr output of command to stderr', async () => {
      const collectorConfig = { run: { command: '$NODE .gen-error-output.js' } }
      const stderr = await captureStderr(() => runScenario({ repoName: 'at-root', collectorConfig }))
      assert.equal(stderr, 'there were some issues\n')
    })

    it('should escape quoted string inside quoted string in command', async () => {
      const collectorConfig = { run: { command: '$NODE -p \'"start\\nall done!"\'' } }
      const stdout = await captureStdout(() =>
        runScenario({
          repoName: 'at-root',
          collectorConfig,
          before: (_, playbook) => {
            delete playbook.runtime.quiet
          },
        })
      )
      assert.equal(stdout, 'start\nall done!\n')
    })

    it('should not send output from command to stdout by default if quiet is set', async () => {
      const collectorConfig = { run: { command: '$NODE .gen-output.js' } }
      const stdout = await captureStdout(() => runScenario({ repoName: 'at-root', collectorConfig }))
      assertx.empty(stdout)
    })

    it('should throw error if command is not found', async () => {
      const collectorConfig = { run: { command: 'no-such-command arg1 "second arg"' } }
      const expectedMessage =
        '(@antora/collector-extension): Command not found: no-such-command ' +
        `(url: http://localhost:${gitServerPort}/at-root/.git | branch: main)`
      assert.throws(await trapAsyncError(() => runScenario({ repoName: 'at-root', collectorConfig })), {
        name: Error.name,
        message: expectedMessage,
      })
    })

    it('should throw error if local command is not found', async () => {
      const collectorConfig = { run: { command: 'no-such-command', local: true } }
      const expectedMessage =
        `(@antora/collector-extension): Command not found: .${ospath.sep}no-such-command ` +
        `(url: http://localhost:${gitServerPort}/at-root/.git | branch: main)`
      assert.throws(await trapAsyncError(() => runScenario({ repoName: 'at-root', collectorConfig })), {
        name: Error.name,
        message: expectedMessage,
      })
    })

    it('should throw error if command is not found for origin with start path', async () => {
      const collectorConfig = { run: { command: 'no-such-command' } }
      const expectedMessage =
        '(@antora/collector-extension): Command not found: no-such-command ' +
        `(url: http://localhost:${gitServerPort}/at-start-path/.git | branch: main | start path: docs)`
      assert.throws(
        await trapAsyncError(() => runScenario({ repoName: 'at-start-path', collectorConfig, startPath: 'docs' })),
        {
          name: Error.name,
          message: expectedMessage,
        }
      )
    })

    it('should throw error if command is not found when using local worktree', async () => {
      const collectorConfig = { run: { command: 'no-such-command' } }
      const expectedMessage =
        '(@antora/collector-extension): Command not found: no-such-command ' +
        `(url: ${ospath.join(REPOS_DIR, 'at-root')} | branch: main <worktree>)`
      assert.throws(await trapAsyncError(() => runScenario({ repoName: 'at-root', collectorConfig, local: true })), {
        name: Error.name,
        message: expectedMessage,
      })
    })

    // also verifies that node base call is replaced with process.execPath
    it('should throw error if command fails to complete successfully', async () => {
      const collectorConfig = { run: { command: '$NODE .gen-failure.js' } }
      const expectedMessage =
        '(@antora/collector-extension): Command failed with exit code 1: ' +
        (windows ? '' : `${process.execPath} .gen-failure.js (url: `)
      assert.throws(await trapAsyncError(() => runScenario({ repoName: 'at-root', collectorConfig })), (err) => {
        assert(err instanceof Error)
        assert.equal(err.name, Error.name)
        assert(err.message.startsWith(expectedMessage))
        return true
      })
    })

    it('should ignore error if command fails when on_failure is ignore', async () => {
      const collectorConfig = { run: { command: 'no-such-command', failure: 'ignore' } }
      assert.doesNotThrow(await trapAsyncError(() => runScenario({ repoName: 'at-root', collectorConfig })))
    })

    it('should log error if command fails when on_failure is log (on_failure is alias for failure)', async () => {
      const collectorConfig = { run: { command: 'no-such-command', on_failure: 'log' } }
      const expectedMessage = `Command not found: no-such-command (url: http://localhost:${gitServerPort}/at-root/.git | branch: main)`
      assert.doesNotThrow(await trapAsyncError(() => runScenario({ repoName: 'at-root', collectorConfig })))
      assert.equal(messages.length, 1)
      const message = messages[0]
      const expectedMessageObj = {
        name: '@antora/collector-extension',
        level: 'error',
        msg: undefined,
      }
      assertx.deepEqualSlice(message, expectedMessageObj)
      const err = message.err
      assert(err instanceof Error)
      assert.equal(err.name, Error.name)
      assert(err.message.includes(expectedMessage))
    })

    it('should log error at specified level if command fails when failure is log.warn', async () => {
      const collectorConfig = { run: { command: 'no-such-command', failure: 'log.warn' } }
      const expectedMessage = `Command not found: no-such-command (url: http://localhost:${gitServerPort}/at-root/.git | branch: main)`
      assert.doesNotThrow(await trapAsyncError(() => runScenario({ repoName: 'at-root', collectorConfig })))
      assert.equal(messages.length, 1)
      const message = messages[0]
      const expectedMessageObj = {
        name: '@antora/collector-extension',
        level: 'warn',
        msg: undefined,
      }
      assertx.deepEqualSlice(message, expectedMessageObj)
      const err = message.err
      assert(err instanceof Error)
      assert.equal(err.name, Error.name)
      assert(err.message.includes(expectedMessage))
    })

    it('should not run collector if no origins are found on content aggregate', async () => {
      await runScenario({
        repoName: 'at-root',
        before: (contentAggregate) => {
          assert.equal(contentAggregate.length, 1)
          delete contentAggregate[0].origins
        },
        after: (contentAggregate) => {
          assertx.empty(contentAggregate[0].files)
        },
      })
    })

    it('should run collector per origin on which it is defined', async () => {
      const repo1 = await createRepository({
        repoName: 'at-root-1',
        fixture: 'at-root',
        collectorConfig: {
          run: { command: '$NODE .gen-start-page.js' },
          scan: { dir: 'build' },
        },
      })
      const repo2 = await createRepository({ repoName: 'at-root-2', fixture: 'at-root' })
      const repo3 = await createRepository({
        repoName: 'at-root-3',
        fixture: 'at-root',
        collectorConfig: {
          run: { command: '$NODE .gen-more-files.js' },
          scan: { dir: 'build' },
        },
      })
      const playbook = {
        content: {
          sources: [
            { url: repo1.url, branches: 'main' },
            { url: repo2.url, branches: 'main' },
            { url: repo3.url, branches: 'main' },
          ],
        },
        runtime: { cacheDir: CACHE_DIR, quiet: true },
        env: process.env,
      }
      const contentAggregate = await aggregateContent(playbook)
      const generatorContext = createGeneratorContext()
      ext.register.call(generatorContext, { config: {} })
      await generatorContext.contentAggregated({ playbook, contentAggregate })
      assert.equal(contentAggregate.length, 1)
      assert.deepEqual(contentAggregate[0].files.map((it) => it.path).sort(), [
        'modules/ROOT/pages/index.adoc',
        'modules/more/pages/index.adoc',
      ])
    })
  })
})
