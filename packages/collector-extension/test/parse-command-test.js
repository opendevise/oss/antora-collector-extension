/* eslint-env mocha */
'use strict'

const { assert, describe, it } = require('@antora/collector-test-harness')
const parseCommand = require('#parse-command')

describe('parseCommand', () => {
  describe('without quoted argument', () => {
    it('should parse single command', () => {
      const command = 'ls'
      const expected = ['ls']
      const actual = parseCommand(command)
      assert.deepEqual(actual, expected)
    })

    it('should parse command with single argument', () => {
      const command = 'antora antora-playbook.yml'
      const expected = ['antora', 'antora-playbook.yml']
      const actual = parseCommand(command)
      assert.deepEqual(actual, expected)
    })

    it('should parse command with multiple arguments', () => {
      const command = 'npx antora antora-playbook.yml'
      const expected = ['npx', 'antora', 'antora-playbook.yml']
      const actual = parseCommand(command)
      assert.deepEqual(actual, expected)
    })

    it('should ignore extra spaces around arguments', () => {
      const command = 'npx  antora   antora-playbook.yml '
      const expected = ['npx', 'antora', 'antora-playbook.yml']
      const actual = parseCommand(command)
      assert.deepEqual(actual, expected)
    })

    it('should parse command with multiple arguments', () => {
      const command = 'npx antora antora-playbook.yml'
      const expected = ['npx', 'antora', 'antora-playbook.yml']
      const actual = parseCommand(command)
      assert.deepEqual(actual, expected)
    })
  })

  describe('with quoted argument', () => {
    it('should parse command with double-quoted argument', () => {
      const command = 'echo "foo bar"'
      const expected = ['echo', 'foo bar']
      const actual = parseCommand(command)
      assert.deepEqual(actual, expected)
    })

    it('should preserve repeating spaces in double-quoted argument', () => {
      const command = 'echo "left  right"'
      const expected = ['echo', 'left  right']
      const actual = parseCommand(command)
      assert.deepEqual(actual, expected)
    })

    it('should parse command with single-quoted argument', () => {
      const command = "echo 'foo bar'"
      const expected = ['echo', 'foo bar']
      const actual = parseCommand(command)
      assert.deepEqual(actual, expected)
    })

    it('should preserve repeating spaces in single-quoted argument', () => {
      const command = "echo 'left  right'"
      const expected = ['echo', 'left  right']
      const actual = parseCommand(command)
      assert.deepEqual(actual, expected)
    })

    it('should parse command with double-quoted argument that contains single quote', () => {
      const command = 'echo "let\'s go!"'
      const expected = ['echo', "let's go!"]
      const actual = parseCommand(command)
      assert.deepEqual(actual, expected)
    })

    it('should parse command with single-quoted argument that contains double quotes', () => {
      const command = 'echo \'"quoted"\''
      const expected = ['echo', '"quoted"']
      const actual = parseCommand(command)
      assert.deepEqual(actual, expected)
    })

    it('should allow double-quoted string to include double quote by escaping it', () => {
      const command = 'echo "use \\" to quote"'
      const expected = ['echo', 'use " to quote']
      const actual = parseCommand(command)
      assert.deepEqual(actual, expected)
    })

    it('should allow single-quoted string to include single quote by escaping it', () => {
      const command = "echo 'use \\' to quote'"
      const expected = ['echo', "use ' to quote"]
      const actual = parseCommand(command)
      assert.deepEqual(actual, expected)
    })

    it('should preserve empty value enclosed in double quotes', () => {
      const command = 'echo "" indented'
      const expected = ['echo', '', 'indented']
      const actual = parseCommand(command)
      assert.deepEqual(actual, expected)
    })

    it('should preserve empty value enclosed in single quotes', () => {
      const command = "echo '' indented"
      const expected = ['echo', '', 'indented']
      const actual = parseCommand(command)
      assert.deepEqual(actual, expected)
    })

    it('should ignore extra spaces between arguments when one of the arguments is quoted', () => {
      const command = 'npx  antora   "my playbook.yml" '
      const expected = ['npx', 'antora', 'my playbook.yml']
      const actual = parseCommand(command)
      assert.deepEqual(actual, expected)
    })
  })

  describe('preserveQuotes: true', () => {
    it('should preserve quotes around double-quoted base call', () => {
      const command = '"my command"'
      const expected = [command]
      const actual = parseCommand(command, { preserveQuotes: true })
      assert.deepEqual(actual, expected)
    })

    it('should preserve quotes around double-quoted argument', () => {
      const command = 'antora --title "My Site" antora-playbook.yml'
      const expected = ['antora', '--title', '"My Site"', 'antora-playbook.yml']
      const actual = parseCommand(command, { preserveQuotes: true })
      assert.deepEqual(actual, expected)
    })

    it('should preserve quotes around single-quoted base call', () => {
      const command = "'my command'"
      const expected = [command]
      const actual = parseCommand(command, { preserveQuotes: true })
      assert.deepEqual(actual, expected)
    })

    it('should preserve quotes around single-quoted argument', () => {
      const command = "antora --title 'My Site' antora-playbook.yml"
      const expected = ['antora', '--title', "'My Site'", 'antora-playbook.yml']
      const actual = parseCommand(command, { preserveQuotes: true })
      assert.deepEqual(actual, expected)
    })
  })
})
