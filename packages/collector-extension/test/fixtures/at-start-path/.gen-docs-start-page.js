'use strict'

const fsp = require('node:fs/promises')

;(async () => {
  await fsp.mkdir('build/docs/modules/ROOT/pages', { recursive: true })
  await fsp.writeFile('build/docs/modules/ROOT/pages/index.adoc', '= Start Page', 'utf8')
})()
