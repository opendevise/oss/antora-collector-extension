'use strict'

const fsp = require('node:fs/promises')

;(async () => {
  await fsp.mkdir('build/modules/ROOT/pages', { recursive: true })
  await fsp.writeFile('build/modules/ROOT/pages/index.adoc', '= Real Start Page\n\nThis is the real deal.\n', 'utf8')
})()
