'use strict'

const fsp = require('node:fs/promises')

;(async () => {
  await fsp.mkdir('build/modules/ROOT/pages', { recursive: true })
  await fsp.writeFile('build/modules/ROOT/pages/index.adoc', '= The Start Page', 'utf8')
  await fsp.mkdir('build/modules/ROOT/partials', { recursive: true })
  await fsp.writeFile('build/modules/ROOT/partials/summary.adoc', 'The summary.', 'utf8')
  await fsp.mkdir('build/modules/ROOT/examples', { recursive: true })
  await fsp.writeFile('build/modules/ROOT/examples/ack.rb', 'puts 1', 'utf8')
})()
