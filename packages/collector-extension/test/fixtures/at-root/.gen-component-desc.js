'use strict'

const fsp = require('node:fs/promises')

;(async () => {
  await fsp.mkdir('build', { recursive: true })
  const antoraYml =
    'name: test\n' +
    'version: 1.0.0\n' +
    (process.argv[2] === 'create' ? 'create: true\n' : '') +
    'title: Test\n' +
    'asciidoc:\n' +
    '  attributes:\n' +
    '    url-api: https://api.example.org\n' +
    `    script-dirname: ${__dirname}\n`
  await fsp.writeFile('build/antora.yml', antoraYml, 'utf8')
})()
