'use strict'

const fsp = require('node:fs/promises')

;(async () => {
  await fsp.mkdir('build', { recursive: true })
  const antoraYml = 'title: Test\n' + 'asciidoc:\n' + '  attributes:\n' + '    url-api: https://api.example.org\n'
  await fsp.writeFile('build/antora.yml', antoraYml, 'utf8')
})()
