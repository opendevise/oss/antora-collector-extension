'use strict'

const fsp = require('node:fs/promises')
const { spawnSync } = require('node:child_process')

;(async () => {
  let version, prerelease
  const branch = spawnSync('git', ['rev-parse', '--abbrev-ref', 'HEAD']).stdout.toString().trimRight()
  if (branch === 'HEAD') {
    version = spawnSync('git', ['describe', '--tags']).stdout.toString().trimRight()
    prerelease = false
  } else {
    version = branch.split('/').pop().replace('.x', '')
    prerelease = true
  }
  if (version.charAt() === 'v') version = version.slice(1)
  await fsp.mkdir('build', { recursive: true })
  const antoraYml = `name: test\nversion: '${version}'\nprerelease: ${prerelease}\ntitle: Test\n`
  await fsp.writeFile('build/antora.yml', antoraYml, 'utf8')
})()
