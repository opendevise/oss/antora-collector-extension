'use strict'

const fsp = require('node:fs/promises')

;(async () => {
  await fsp.mkdir('build/many-pages', { recursive: true })
  const count = Number(process.argv[2] ?? '20')
  for (let n = 1; n <= count; n++) {
    const npad = n < 10 ? '0' + n : String(n)
    await fsp.writeFile(`build/many-pages/${npad}.adoc`, `= Page ${n}`, 'utf8')
  }
})()
