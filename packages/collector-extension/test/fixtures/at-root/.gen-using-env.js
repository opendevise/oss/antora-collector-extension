'use strict'

const fsp = require('node:fs/promises')

;(async () => {
  await fsp.mkdir('build/modules/ROOT/pages', { recursive: true })
  let contents
  if ('ENV_VALUE_AS_STRING' in process.env) {
    const envData = JSON.stringify({ ENV_VALUE_AS_STRING: process.env.ENV_VALUE_AS_STRING, USER: process.env.USER })
    contents = `= Environment Variables\n\n ${envData}`
  } else if ('refname' in process.env) {
    contents = `= Refname\n\n${process.env.refname}`
  } else {
    const origin = JSON.parse(process.env.ANTORA_COLLECTOR_ORIGIN || '{}')
    const playbook = JSON.parse(process.env.ANTORA_PLAYBOOK || '{}')
    contents = [
      '= Origin Info',
      '',
      'reftype:: ' + origin.reftype,
      'refname:: ' + origin.refname,
      'worktree:: ' + process.env.ANTORA_COLLECTOR_WORKTREE,
      'site title:: ' + process.env.ANTORA_COLLECTOR_SITE_TITLE,
      'site url:: ' + playbook.site.url,
      'env in playbook:: ' + ('env' in playbook),
    ].join('\n')
  }
  await fsp.writeFile('build/modules/ROOT/pages/index.adoc', contents, 'utf8')
})()
