'use strict'

const fsp = require('node:fs/promises')

;(async () => {
  await fsp.writeFile('path/does/not/exist/file.adoc', 'content', 'utf8')
})()
