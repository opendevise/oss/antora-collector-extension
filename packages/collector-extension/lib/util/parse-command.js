'use strict'

const QUOTE_RX = /["']/
const SPACES_RX = / +/

const parseCommand = (cmd, { preserveQuotes } = {}) => {
  if (!QUOTE_RX.test((cmd = cmd.trim()))) return cmd.split(SPACES_RX)
  const chars = [...cmd]
  const lastIdx = chars.length - 1
  return chars.reduce(
    (accum, c, idx) => {
      const { tokens, token, quotes } = accum
      if (c === "'" || c === '"') {
        const quote = quotes.get()
        if (quote) {
          if (c === quote) {
            if (token[token.length - 1] === '\\') {
              token[token.length - 1] = c
            } else {
              if (preserveQuotes) token.push(c)
              tokens.push(token.length ? token.join('') : '')
              token.length = quotes.clear() || 0
            }
          } else {
            token.push(c)
          }
        } else {
          if (preserveQuotes) token.push(c)
          quotes.set(undefined, c)
        }
      } else if (c === ' ') {
        if (quotes.get()) {
          token.push(c)
        } else if (token.length) {
          tokens.push(token.join(''))
          token.length = 0
        }
      } else {
        token.push(c)
      }
      if (idx === lastIdx && token.length) tokens.push(token.join(''))
      return accum
    },
    { tokens: [], token: [], quotes: new Map() }
  ).tokens
}

module.exports = parseCommand
