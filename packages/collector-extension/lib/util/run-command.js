'use strict'

const fs = require('node:fs')
const { promises: fsp } = fs
const ospath = require('node:path')
const parseCommand = require('./parse-command')
const { execFile, spawn, spawnSync } = require('node:child_process')

const invariably = { true: () => true, false: () => false }

const ENV_NAME_RX = /\$(\w+)/g
const IS_WIN = process.platform === 'win32'
const WIN_BATCH_USE_SHELL = IS_WIN && spawnSync('npm.cmd', ['-v'], { windowsHide: true }).error?.code === 'EINVAL'
const WIN_SHELL_ESCAPE_CHARS = { ' ': true, '"': true, '<': true, '>': true, '&': true, '%': true, '^': true }

async function runCommand (cmd = '', opts = {}) {
  const shell = opts.shell
  let cmdv = parseCommand(String(cmd), { preserveQuotes: shell })
  if (!cmdv.length) throw new TypeError('Command not specified')
  let cmd0 = cmdv[0]
  const { quiet, local, cache: { where = {} } = {}, ...spawnOpts } = opts
  if (IS_WIN) {
    if (~cmd0.indexOf('/')) cmdv[0] = cmd0 = cmd0.replace(/[/]/g, ospath.sep)
    if (shell) {
      cmdv = cmdv.map((it) => (~it.indexOf('$') ? it.replace(ENV_NAME_RX, '%$1%') : it))
    } else {
      const bare = !~cmd0.indexOf(ospath.sep)
      let extname = ospath.extname(cmd0)
      if (extname) {
        if (bare && local) cmdv[0] = '.' + ospath.sep + cmd0
      } else if (bare && !local) {
        if (cmd0 in where) {
          extname = ospath.extname((cmdv[0] = where[cmd0]))
        } else {
          cmdv[0] = await new Promise((resolve) => {
            execFile('where', [cmd0 + '.???'], { cwd: opts.cwd, windowsHide: true }, (_, results) => {
              if (!(results = results.trimEnd())) return resolve(cmd0)
              const exts = results.split('\r\n', 2).reduce((accum, it) => {
                const ext = ospath.extname(it)
                return ext ? Object.assign(accum, { [ext.slice(1)]: ext }) : accum
              }, {})
              resolve(cmd0 + (extname = exts.exe || exts.bat || exts.cmd || ''))
            })
          })
        }
      } else {
        let absCmd0 = cmd0
        if (!ospath.isAbsolute(absCmd0)) {
          absCmd0 = ospath.join(opts.cwd || '', cmd0)
          if (bare) cmdv[0] = '.' + ospath.sep + cmd0
        }
        for (const resolvedExt of ['.exe', '.bat', '.cmd']) {
          if (!(await fsp.access(absCmd0 + resolvedExt).then(invariably.true, invariably.false))) continue
          cmdv[0] = cmd0 + (extname = resolvedExt)
          break
        }
      }
      if (WIN_BATCH_USE_SHELL && (extname === '.bat' || extname === '.cmd')) {
        cmdv = cmdv.map(winShellEscape)
        spawnOpts.shell = true
      }
    }
    Object.assign(spawnOpts, { windowsHide: true, windowsVerbatimArguments: false })
  } else if (local && !~cmd0.indexOf('/')) {
    cmdv[0] = './' + cmd0
  }
  return spawnCommand(cmdv, spawnOpts, quiet)
}

async function spawnCommand (cmdv, opts, quiet) {
  return new Promise((resolve, reject) => {
    try {
      const [cmd, ...args] = cmdv
      const stderr = []
      const ps = spawn(cmd, args, opts)
      ps.on('close', (exitCode, signalCode) => {
        if (exitCode === 0) {
          if (stderr.length) process.stderr.write(stderr.join(''))
          resolve()
        } else {
          const result = signalCode ? `terminated with signal ${signalCode}` : `failed with exit code ${exitCode ?? -1}`
          let msg = `Command ${result}: ${ps.spawnargs.join(' ')}`
          if (stderr.length) msg += '\n' + stderr.join('')
          reject(new Error(msg))
        }
      })
      ps.on('error', (err) => reject(err.code === 'ENOENT' ? new Error(`Command not found: ${ps.spawnargs[0]}`) : err))
      ps.stdout.on('data', (data) => quiet || process.stdout.write(data))
      ps.stderr.on('data', (data) => stderr.push(data))
      ps.stdin.end()
    } catch (err) {
      reject(err)
    }
  })
}

const winShellEscape = (val) => {
  const result = []
  for (const c of val) {
    if (WIN_SHELL_ESCAPE_CHARS[c]) {
      result[result.length - 1] === '"' ? result.pop() : result.push('"')
      result.push(c, '"')
    } else {
      result.push(c)
    }
  }
  return result.join('')
}

module.exports = runCommand
