= Configure a Clean Action
:navtitle: Clean

Since the purpose of Collector is to generate files to be imported, it may be necessary to clean the worktree from a previous run to avoid outdated files from being imported.
A clean action can clean the worktree between two Collector steps or, in the case the content source is a local or otherwise preserved worktree, before Collector runs on the origin.

You can configure one or more clean actions using the `clean` key.
This page explains how to use the `clean` key and the keys it accepts.

== clean key

A clean action is configured using the `clean` configuration key for the Collector instance.
The `clean` key must be nested under the `collector` key.

.antora.yml
[,yaml]
----
ext:
  collector:
    clean:
      dir: build-foo
----

If the value of the `collector` key is an array, the `clean` key must be specified as a key on an array entry.
The `clean` key may be used in more than one entry in that array.

.antora.yml
[,yaml]
----
ext:
  collector:
  - clean:
    - dir: build-foo
    - dir: build-bar
  - clean:
      dir: build-baz
----

When the content source is local and has a worktree, the worktree may contain files that are untracked or ignored by git.
In this case, the clean action on the first step can be important to ensuring a predictable result.
Clean actions on other steps may be used to remove intermediate files generated from a previous step.

The value of the `clean` key can be map, an array, or a string.
If the value is a string, the value is assumed to the value of the `dir` key of a map.
If the value is an array (i.e., list of entries), each entry must be a string or a map consisting of built-in key-value pairs.
If the value is a map (i.e., the leading `-` marker is dropped), it's assumed to be a single-entry array.

Acceptable keys for the map value are listed in the table below.

[cols=m;1;2;3]
|===
|Key |Default |Type |Description

|<<dir-key,dir>>^*^
|undefined
|String (absolute path, path relative to start path if starts with `./`, or path relative to content source root)
|The path of the directory to remove.
|===

{empty}* required

== dir key

You configure a clean action by specifying a directory to clean using the `dir` key.
The `dir` key defines the path of a directory to clean for this step in the Collector instance.
This key is required.

The directory specified by the `dir` key will be completely removed.
If the path is absolute, that value is used as is.
If the path begins with `./` (e.g., `./docs`), the `./` segment will resolve to the start path within the worktree.
Otherwise, the path is resolved relative to the worktree directory.

.antora.yml
[,yaml]
----
name: colorado
title: Colorado
version: '5.6.0'
ext:
  collector:
    clean:
      dir: build/generated
----

TIP: If the directory to clean is the same as the directory to scan, the clean entry can be created implicitly by setting the `clean` key on the scan entry to `true` (e.g., `clean: true`).

Each `clean` entry is invoked sequentially in the order specified in the array.

== Implicit clean

When running collector on a content source without a worktree (such as a remote repository), Collector will create a temporary one.
By default, the worktree will be removed once Collector is finished using it.
In this scenario, the worktree will always start out in a clean slate.

If the `keep_worktrees` option on the extension is set to `true`, the worktree from a previous run will be reused and thus not guaranteed to be in a clean state.
If the `keep_worktrees` option is either `true` or a specified event (e.g., `until:contextClosed`), the location of the worktree will be assigned to the `collectorWorktree` property on the origin and thus available to other extensions.
This property is always set in the case that Collector reuses the local worktree.
