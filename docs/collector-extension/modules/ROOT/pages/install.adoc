= Install the Extension
:navtitle: Install

The Antora Collector extension requires Node.js {version-node-min} or greater and Antora {version-antora-min} or greater.
//No external commands are required to use Collector except for the user-specified ones in a Collector instance.
No external commands are required, by default.
The only external commands required are the ones that Collector is configured to run using the run action of a Collector instance.

== Prerequisites

The instructions in the following sections assume you've already set up a playbook project, an Antora playbook file (i.e., [.path]_antora-playbook.yml_), and that you're running the commands from your playbook project.

=== Node.js

This software requires Node.js {version-node-min} or greater.
To see if you have Node.js installed, and which version, type:

 $ node -v

You should see a version string, such as:

[subs=attributes+]
 $ node -v
 v{version-node}

If no version number is displayed in your terminal, you need to install Node.js.
Follow one of these guides to learn how to install nvm and Node.js on your platform.

* xref:antora:install:linux-requirements.adoc#install-nvm[Install nvm and Node.js on Linux]
* xref:antora:install:macos-requirements.adoc#install-nvm[Install nvm and Node.js on macOS]
* xref:antora:install:windows-requirements.adoc#install-choco[Install nvm and Node.js on Windows]

=== Antora

The Antora Collector extension requires Antora {version-antora-min} or greater.
To confirm you have a suitable version of Antora installed, run:

 $ npx antora -v

The command should report the version of the Antora CLI and site generator you have installed.
If you're not yet using Antora {version-antora-min} or greater, you need to xref:antora:install:upgrade-antora.adoc[upgrade] to use the Collector extension.

Once you've satisfied the prerequisites, you're ready to install the Collector extension in your playbook project.

== Install the extension package

To install the package for the Antora Collector extension in your playbook project, type the following command:

 $ npm i @antora/collector-extension

This command installs the extension package alongside Antora and any other extensions or libraries you're using in your playbook project.

IMPORTANT: We strongly recommend that you install Antora, this extension, and any other extensions that you use in your xref:antora:playbook:use-an-existing-playbook-project.adoc[playbook project], the directory where your site's Antora playbook file (e.g., [.path]_antora-playbook.yml_) is located.
If you install the npm packages globally, they can interfere with other software install on your system and conflicts may occur.

Now that the extension is properly installed, you need to xref:register.adoc[register the extension] in your playbook.
